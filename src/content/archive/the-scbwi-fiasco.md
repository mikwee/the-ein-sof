---
title: The SCBWI Fiasco
description: A summary of the controversy surrounding the SCBWI's statement
tags:
- politics
- judaism
- israel
- far-left
author: mikwee
authorTwitter: toon_mikwee
date: "2021-12-28"
category: politics
---

This story has been driving me crazy every since it started, and even after it ended. Now that I have an idea what I wanna do with this blog, it’s time I tell it all.

First, I should explain what the SCBWI even is. Basically, it’s an acronym for the Society of Children’s Book Writers and Illustrators. Pretty self-explanatory name, isn’t it? It should be an uncontroversial organization, and it was. However, everything changed on June 10th 2021, with one statement condemning Judeophobia. I came by the SCBWI’s Twitter by mere chance, and when I saw that statement, I was ecstatic! However, after I QRTd with a positive comment, somebody commented on my tweet, claiming the organization “mistreated a Palestinian writer”. I was intrigued, and had a “meh” feeling at first. However, the more I dived in, the more I was infuriated by this story. And now, let’s actually tell it.

## Chapter 1: A Jewish Story I Didn’t Write

At the time, the Jewish community was suffering from a [wave of Judeophobic attacks](https://edition.cnn.com/2021/06/09/us/tucson-synagogue-anti-semitic-graffiti/index.html), a lot due to the conflict between Israel and Hamas last May. While a lot remained silent, the SCBWI, and its Equity &amp; Inclusion Officer, black Jew April Powers, were not. They released the following statement, on both Twitter and Facebook:
![image](https://64.media.tumblr.com/857565ef7a47d7eb2a1dfc5650923980/89e608261c5a0a33-a2/s540x810/083230aa449b39bd1641c650b88584d25a0af9da.jpg)

As expected, most comments were positive. However, everything changed when Razan Abdin-Adnani atta… eh, [commented](https://nitter.net/razanabnani/status/1403087011855089667).
![image](https://64.media.tumblr.com/67cfc1f9d8ac77d52464cc59bf809487/89e608261c5a0a33-8a/s540x810/63c8278ed9faf8d385c870c61603b8ab3d88b533.jpg)

This is suspicious at best. Imagine if somebody tweeted their support for BLM, and I commented with “What about Jews?!”. That would be me All Lives Mattering the situation, thus shifting away focus from the fight for Black rights. Notice how the statement doesn’t even mention Israel or Palestine. Just Jews. Razan [commented yet again](https://nitter.net/razanabnani/status/1403090476996177923). Powers responded as such, and Razan [responded again](https://nitter.net/razanabnani/status/1403105498791038981):
![image](https://64.media.tumblr.com/583250517d464eda80a6d5a13b78bdd4/89e608261c5a0a33-54/s540x810/8b86a9e9f981a5c53c24c42b33b72822943031ea.jpg)

This is the only part where I can say, ugh… in defense of Razan, Islamphobia is a big problem, and if there was a surge, it would’ve been nice for the SCBWI to address it. However, this does not excuse Razan’s ALM method of activism, and everything else we will discuss below.

This resulted in people attacking the SCBWI and Powers specifically. These attacks has [continued for long after, even including death threats](https://www.newsweek.com/april-powers-ex-scbwi-diversity-chief-sent-death-threats-after-sharing-antisemitism-post-1607181), forcing her to resign, purely to not deal with this shit. Eventually, the SCBWI admitted defeat by deleting the statement from Twitter (but not from Facebook), and posting [this apology](https://nitter.net/scbwi/status/1409262124547727372):
![image](https://64.media.tumblr.com/877509f774eec3879ab67e98778a8c60/89e608261c5a0a33-f9/s540x810/ddb7c4101bdaa7f69d9ec5dcc3d3d5f850862da9.jpg)

Now, my question is… What the fuck was there to apologize for? Acknowledging Judeophobia? Not listening to an ALM wannabe? By the way, this apology wasn’t enough for Razan, as she wanted a personal apology from Powers. Seriously, what a brat. Understandably, this did **not** go well with the Jewish community, which collectively panned the move. But believe me, we’ve only scratched the surface. It’s time for us to learn who Razan Abdin-Adnani truly is.

## Chapter 2: Razan and the Myth of Allyship

As she explained in her first comment, Razan is an author who recently joined the SCBWI at the time of the incident. In that same comment, she also claims that the violence against “our Jewish siblings” is “very real”. In other words, she is portraying herself as an ally to the Jewish community. But has she proved herself as an ally in the past? Let’s take a look back.

In May 2021, about a week after the conflict ended, Razan [called on the “Zionists”](https://nitter.net/razanabnani/status/1400225183324061697) to go back to… the places that persecuted, and tried to kill them. Very charming.
![image](https://64.media.tumblr.com/398f18965a1c190e329f9a4e37f7a0dd/89e608261c5a0a33-1f/s540x810/f7abea224d407edcae7c01df72f8abed0a124c8a.jpg)

Later, while her fight against the SCBWI was **ongoing**, Razan [criticized](https://nitter.net/razanabnani/status/1403693907171590144) House Speaker Nancy Pelosi for… visiting a café [vandalized by Judeophobes](https://forward.com/news/471020/san-francisco-deli-mannys-defaced-with-antisemitic-graffitiagain/).
![image](https://64.media.tumblr.com/46bed29cb06c10270d904458e9bee5ab/89e608261c5a0a33-8a/s540x810/8cbf6eaf2e1c71639361307a3c8ce7de9e7c33ee.jpg)

This is the “anti-Zionist” graffiti, by the way.
![image](https://64.media.tumblr.com/304be36aefeee1d12e993749064e2bc1/89e608261c5a0a33-e6/s540x810/1b259e592ca1537d7addb0745f411d3feb8ce099.jpg)

By now, I hope I have managed to establish that Razan is a raving Judeophobe.

## Chapter 3: Cooling Down &amp; Epilogue

So let’s sum this series of events all up:

1.  A professional association posted a statement supporting the Jewish community at their time of need.
2.  A Judeophobic member of the organization tried to ALM it up by shifting attention from Jews to her community.
3.  The organization’s officer responsible for the statement was harassed and threatened.
4.  The officer resigned to stop this wave of hatred, which didn’t happen.
5.  The organization apologized for the statement.

I think I can say that I rest my case.
