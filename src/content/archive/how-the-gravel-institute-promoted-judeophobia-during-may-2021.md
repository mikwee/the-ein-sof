---
title: How the Gravel Institute promoted Judeophobia during May 2021
description: An analysis of a cherrypicking tweet by the Gravel institute
tags:
- politics
- judaism
- israel
- far-left
author: mikwee
authorTwitter: toon_mikwee
date: "2021-10-20"
category: politics
---
**DISCLAIMER** (11.3.2023): These days, I am not sure whether I still endorse all things said in this article. However, I still stand by my rebuke of IMEU's (and by extension, the Gravel Institute's) cherrypicking and double standards, and I plan to keep this article up for preservation.

![The Gravel Institute tweeted on May 15th, 2021: "These are all real quotes from senior Israeli officials."](https://64.media.tumblr.com/5e2eced149f58aa00afe9b3418adc202/fcc5d28b7ecb9010-a7/s540x810/2621f5d5aed84a4331f44ac5bbb9b829801edb0f.jpg)

## Level 1: Post-Argument

A lot of people are talking about the “post-truth” era nowadays. That is worrying and important, but I think this tweet represents what I call the “post-argument” era. You see, instead of coherent arguments, too many players nowadays rely on fallacious demagoguery. In this case, IMEU, which Gravel references here, is cherrypicking quotes and taking them out of context to make an entire country seem like an imperial fascist state… which you could do to any country, including Palestine.

## Level 2: Conflict

This brings us to the quotes themselves, and well… let’s just analyze them, one by one!

1.  Eli Ben-Dahan [did say that “Palestinians are beasts”](https://www.mekomit.co.il/%d7%90%d7%9e%d7%a8%d7%aa-%d7%a9%d7%94%d7%a4%d7%9c%d7%a1%d7%98%d7%99%d7%a0%d7%99%d7%9d-%d7%9c%d7%90-%d7%91%d7%a0%d7%99-%d7%90%d7%93%d7%9d-%d7%a7%d7%91%d7%9c-%d7%90%d7%97%d7%a8%d7%99%d7%95%d7%aa-%d7%a2/), but well… he hasn’t been politically active since 2019. A lot has changed since then.
[Ayelet Shaked’s post]
2.  Now, unlike Eli, Ayelet Shaked is still politically active, even serving as Minister of Interior since last June, so this is getting interesting. Ayelet quoted an editorial by the late Uri Elitzur in a now-deleted Facebook post, so it’ll be inaccurate to quote this as her words… but she did endorse it! The [editorial in question](https://www.inn.co.il/news/506801) was apparently written in 2002, according to Arutz Sheva, and calls the entire Palestinian people “the enemy”, and basically justifies attacking civilians because “they started”, basically a very brutal editorial. Shaked did try to excuse this in a [Jerusalem Post editorial](https://www.jpost.com/opinion/op-ed-contributors/exposing-militant-leftist-propaganda-363062), but she’s still looking pretty bad, tho I am more chill purely because this all happened in 2014. You know what, GI &amp; IMEU? You get a point.
3.  Yes, Miri Regev is a right-wing populist who did call Sudanese immigrants “a cancer in out body”, but while there are anti-immigrant movement in Israel, this extreme cancer comparison is far from the consensus. Most Israelis were disgusted by this remark. Moving on!
4.  Lieberman has said that “unloyal” Arabs should have their head chopped in 2015, but some time has passed since then, and he has remarkably stayed quiet on Arab/Palestinian issues, focusing on separation of religion and state and economic issues (He is the current Minister of Finance, after all). While I still do not like the man, and I do wish Lieberman faced the consequences of some of his older statements, this is far from being representative of most Israelis I know.
5.  Moshe Feiglin did, in fact, call for what is practically an [ethnic cleansing of Gazans](https://www.israelnationalnews.com/Articles/Article.aspx/15326). However, like Eli, has also been irrelevant for some time now. In fact, he hasn’t been in the Knesset since 2015 (!), even a longer time than Eli. He did lead his own party, the right-libertarian Zehut, but it hasn’t entered the Knesset in the April 2019 elections, and has been falling apart ever since, with Feiglin himself returning to the Likud last March. Feiglin is nowhere near influential in 2020s Israel.
6.  Another Eli Ben-Dahan quote! Yes, he [did say that](https://www.makorrishon.co.il/nrg/online/1/ART2/534/750.html), but again, he’s not relevant. Moving on!
7.  Okay, this is just stupid. Do you really think Miri Regev seriously admitted to being a fascist? She's dumb… but not that dumb! This all happened in 2012, when Regev [debated then-MK Jamal Zahalka](https://youtu.be/cTUBoUOfRzk?t=545). When he called her a fascist, Regev dismissively remarked - or more accurately, shouted - “I’m happy to be a fascist, if you call me a fascist”. Now, I absolutely hate Miri Regev. I think she is an opportunistic populist who wages war against anything that can give her a political advantage. I even think some of her rhetoric has far-right undertones. But saying she outright admitted to be a fascist is a _real_ stretch. This is the first outright wrong quotation here.
8.  Yes, Tzipi Hotovely, current ambassador to the UK, has [warned of assimilation](https://outline.com/KUfba8), but to be honest… This is a very lame accusation! I’m not that scared of assimilation, but sadly, many Jews are scared of diaspora Jewry disappearing because of it. Some people inside Israel _are_ afraid of assimilation because of Arabophobia, but there are other reasons.
9.  Guess what? Unlike all the other quotes, Yaalon actually [apologized](https://www.globes.co.il/news/article.aspx?did=617416) for this one, saying it was taken out of context, but also that it was not a good statement.
10.  Finally, we have the man himself! And… [Yeah, I can’t salvage this one](https://www.tabletmag.com/sections/news/articles/fibi-netanyahu).

## Level 3: The Wind-Down

Overall, we have one quote that has been regretted, one quote that was taken out of context, one more quote that many not be related to Arabophobia at all, two that were actually quite upsetting, and five that… don’t really matter in the big picture. The thing is, the New Yorkers behind the Gravel Institute, and arguably IMEU, have no idea idea who these people are, or how much impact their statements have. These people don’t affect their lives, but they do affect mine.
![image](https://64.media.tumblr.com/a18797efbe0730aeb802fc8a011f1a77/fcc5d28b7ecb9010-a7/s540x810/ca7cbacb8cc6d78e79055bc502cd56c5c22d28bc.jpg)

Because you see, far-leftists like GI love throwing the term “anti-war” or even “pacifism” around, but that only refers to ending _US involvement_ in wars, not ending the wars themselves. These people are not interested in us, promoting the Israeli left, or making life better for people in the region. They simply say “Israel is shit”… and that’s it. So what do we get with that logic? Judeophobia and demonization, of course.

## Level 3: Hatred Enriched
![image](https://64.media.tumblr.com/04d8189d0fd82cc1b9400bf4e45eb9c5/fcc5d28b7ecb9010-46/s540x810/b6799729b1a70ef4a7ec2929f5d435f5aa1c35dc.jpg)

What you just saw were two comments on Gravel’s tweet. Both are disturbingly Judeophobic, as they are examples of [Holocaust inversion](https://instagram.com/p/CQd82kABy6-). Holocaust inversion is equating Jews to the Nazis. It is problematic in many ways, all explained in Debbie’s post I linked, but basically, it minimizes the Holocaust, and also compares the vast majority of Holocaust survivors to the people who tried to murder them. It’s truly vile.

Throughout the 11 days of Operation Guardian of the Walls, in May 2021, I was scared as fuck. Not only was I under threat of rocket attacks - even if the Iron Dome stopped them, the trauma still forms - but I also had to bear with everyone on social media, especially Twitter, dismissing my trauma and fear as “colonialist”, and constantly spreading one-sided retelling of a very complex story. This only made the trauma worse.

This demonization of Israelis is not helping anyone, and it's not far from the "all Palestinians are terrorists" mentality, common in right-wing Zionist circles.

Gravel can hide behind empty words, or the AsaJews they brought to manage their Twitter page, but it won't be able to erase the fact that they spread misleading material, defend Judeophobes like Jeremy Corbyn and shit about a conflict they have no idea about. Their rhetoric, at least in my eyes, is dangerous.

Wait, you know what? Forget what I said. They're 🌈progressive🌈! That makes them good!

-END-
