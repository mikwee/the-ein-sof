---
title: How I implemented the logo (and more)
description: An untraditional welcome to a new blog
tags:
- programming
- web dev
- fonts
- logos
author: mikwee
authorTwitter: toon_mikwee
date: "2024-3-15"
category: programming
rtl: false
---

Welcome to **The Ein Sof** - my old-new blog! Finally, I feel satisfied with what is hosted on this domain. I plan to post here whatever I want, whenever I want, with no constraints. I just hope people read what I have to write - I need the *tsumi* (short for "attention" in Hebrew). This time round, the site is using an Astro theme called [Astro Ink](https://github.com/one-aalam/astro-ink).

However, I gotta share that I faced some troubles in relaunching this site.

First, I wanted to make the site's logo - taken from [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Ein_Sof1.jpg) consistent with the dark and light themes - white in dark theme, black in light theme. However, that is very tricky to do with an SVG, given Astro Ink's dependence on Tailwind CSS for styling. However, a [web dev chatroom on Matrix](https://matrix.to/#/#efy_webdev:matrix.org) proposed I use an icon font instead - a magic solution! With an icon font, you can use Tailwind to change the color based on the theme. I definitely recommend this approach.

But then, I faced another issue. I wanted to create an archive section of the site, for past posts that do not necessarily reflect the person I am today, but I still want to keep for preservation puposes. But then I the components for the post list - `PostPreview` for the post listing itself and `PostPreviewList` for creating a column of these posts - did not exactly work. After some time I managed to figure this out by myself - while I added a `collection` prop to both components (the collection can be `blog`, `archive` or anything else I might want to add in the future), only `PostPreviewList` needs to have `blog` as a default value, if I want a default value. So now, everything works perfectly! Isn't this site beautiful!

Anyways, thank you for reading, and I hope you join me on my new blogging journey!



Edit (6.4.2024): After this post was finished, I faced another major problem: Deploying the site to Codeberg Pages! I messed around with CI/CD, before a person on the aforementioned chatroom told me I can use the [`gh-pages`](https://github.com/tschaub/gh-pages) to push to a custom remote.
