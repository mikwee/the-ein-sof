export function getAge(dob) {
  const today = new Date();
  const birthday = new Date(dob.getFullYear(), dob.getMonth(), dob.getDate());
  const yearsDiff = today.getFullYear() - birthday.getFullYear();

  if (today < new Date(today.getFullYear(), dob.getMonth(), dob.getDate()))
  {
    return yearsDiff - 1;
  } else {
    return yearsDiff;
  }
}
